import Vue from 'vue'
import App from './App.vue'

import StartScreen from './assets/StartScreen.vue';
import Questions from './assets/Questions.vue';
import ResultScreen from './assets/ResultScreen.vue';



Vue.component('StartScreen', StartScreen);
Vue.component('Questions', Questions);
Vue.component('ResultScreen', ResultScreen);


new Vue({
  el: '#app',
  render: h => h(App)
});

